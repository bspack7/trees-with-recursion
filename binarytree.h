#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <algorithm>
#include <limits>


template <class Etype>
class TreeNode
{
public:
  Etype element;  // value in node
  TreeNode *left;  // left child
  TreeNode *right; // right child
  TreeNode *parent; // parent of node
  TreeNode(Etype e = 0, TreeNode *l = NULL, TreeNode *r = NULL, TreeNode *p = NULL) :
          element(e), left(l), right(r), parent(p) { }

};

template <class Etype>
class BinaryTree
{
protected:
  TreeNode<Etype> *root;  // root of tree
  Etype LOW;   // Smallest possible value stored in tree
  Etype HIGH;  // Largest possible value stored in tree
  bool insert(Etype & x, TreeNode<Etype> * & t, TreeNode<Etype>*  p);

  void printTree(TreeNode<Etype> * t, std::string indent, int currDepth = 0, int maxDepth = std::numeric_limits<int>::max());

  //TreeNode<Etype> * findFirst(TreeNode<Etype> * t) const;
  //bool findNumber(TreeNode<Etype> * subroot, Etype searchNode);
  int size(TreeNode<Etype> *t) const;
  TreeNode <Etype>* currentNode;
  //TreeNode <Etype>* successor(TreeNode<Etype> * searchNode);
  //void printInorder(TreeNode<Etype> * node);
  //void deleteNode(TreeNode<Etype> *& node);
  //void closestCommon(TreeNode<Etype> * subroot, Etype t1, Etype t2);
  //Etype closestCommonHelper(Etype t1, Etype t2);

public:
  Etype getCurrNodeValue( );
  void  printTree(std::string msg, int depth = std::numeric_limits<int>::max())
  {
    std::cout << std::endl << msg << std::endl;
    printTree(root, "", 0, depth);
  }


  bool findNumber(TreeNode<Etype> * subroot, Etype searchNode)
  {
    if (subroot == NULL)
    {
      return false;
    }

    if (subroot->element == searchNode)
    {
      return true;
    }

    if (searchNode < subroot->element)
    {
      return findNumber(subroot->left, searchNode);
    }

    else
    {
      return findNumber(subroot->right, searchNode);
    }
  }


  Etype closestCommonHelper(Etype t1, Etype t2)
  {
    if (findNumber(root,t1) && findNumber(root,t2))
    {
      return closestCommon(root, t1, t2);
    }
    return NULL;
  };

// Find the leftmost node in a tree rooted at t.

  TreeNode<Etype> * findFirst(TreeNode<Etype> * t) const
  {
    if (t == nullptr) return t;
    TreeNode<Etype>* s = t;
    for (; s->left != NULL; s = s->left)
      ;
    return s;
  }


  TreeNode<Etype> * successor(TreeNode<Etype> *searchNode) {
    if (searchNode->right != NULL)
    {
      return findFirst(searchNode->right);
    }

    auto tempNode = searchNode->parent;

    while (tempNode != NULL && searchNode == tempNode->right)
    {
      searchNode = tempNode;
      tempNode = tempNode->parent;
    }
    return tempNode;
  }


  void printInorder(TreeNode<Etype> * node)
  {
    if(node != NULL)
    {
      printInorder(node->left);
      std::cout << node->element << std::endl;
      printInorder(node->right);
    }
  }


  void deleteNode(TreeNode<Etype> *node) {
    //
    if (node == NULL)
    {
      return;
    }
    //check if node-> left exists
    if(node->left != NULL)
    {
      if (node->left->left == NULL && node->left->right == NULL) {
        delete node->left;
        node->left = nullptr;
      }
      deleteNode(node->left);

    }

    if (node->right != NULL)
    {
      if (node->right->left == NULL && node->right->right == NULL) {
        delete node->right;
        node->right = nullptr;
      }
      deleteNode(node->right);
    }

  }


  Etype closestCommon(TreeNode<Etype> *subroot, Etype t1, Etype t2) {

    // O(n) = nlog(n)

    if (t1 < subroot->element && t2 < subroot->element)
    {
      return closestCommon(subroot->left, t1, t2);
    }

    else if (t1 > subroot->element && t2 > subroot->element)
    {
      return closestCommon(subroot->right, t1, t2);
    }

    else
    {
      return subroot->element;
    }
  }

  /* Return the leftmost value in the tree.  Set a local currentNode to that node.*/
  Etype getFirst() {
    currentNode = findFirst(root);
    if (currentNode == NULL) return LOW;
    return currentNode->element;
  }

  bool isBST()
  {
    // O(n) = log(n)

    isBSThelper(root,INT_MIN,INT_MAX);
  }

  bool isBSThelper(TreeNode<Etype> * subroot, int min, int max)
  {
    if (subroot == NULL)
    {
      return true;
    }

    min = subroot->element + 1;
    max = subroot->element - 1;

    if (subroot->element > max || subroot->element < min)
    {
      return false;
    }

    else
    {
      return isBSThelper(subroot->left,min,subroot->element - 1) && isBSThelper(subroot->right,subroot->element + 1,max);
    }

  }

  void printShort(std::string message)
  {
    // O(n) = log(n)
    std::cout<< message << std::endl;
    printInorder(root);
  }


  BinaryTree(Etype low = 0, Etype high = 0) : root(NULL) {
    LOW = low;
    HIGH = high;
  }

  void makeEmpty()
  {
    while (root != NULL)
    {
      fall();
    }

  }

  //find the successor to currentNode.  Change currentNode to point to the successor.
  void successor()
  {
    // O(n) = nlog(n)
    currentNode = successor(currentNode);
  }

  int widthHelper(TreeNode<Etype> * t, int & height)
  {
    if (t == NULL)
    {
      height = -1;
      return 0;
    }
    int leftHeight = 0;
    int rightHeight = 0;
    int leftWidth = widthHelper(t->left, leftHeight);
    int rightWidth = widthHelper(t->right, rightHeight);

    height = std::max(leftHeight,rightHeight);
    return std::max(leftHeight + rightHeight + 2, std::max(leftWidth, rightWidth));
  }

  int width()
  {
    // O(n) = log(n)

    int height;
    return widthHelper(root, height);
  }

  void getMaxCompleteSubtree() {}

  void perfectBalance(Etype *list, int beg, int final)
  {
    // O(n) = nlog(n)

    if (beg > final)
    {
      return;
    }
    int mid = (beg + final)/2;
    insert(list[mid]);
    perfectBalance(list,beg,mid-1);
    perfectBalance(list,mid+1,final);
  }

  void fall()
  {
    // O(n) = nlog(n)
    deleteNode(root);
  };

  void flip() {
    // O(n) = nlog(n)

    flipHelper(root);
  }

  void flipHelper(TreeNode<Etype> * node) {
    if (node == NULL)
    {
      return;
    }
    flipHelper(node->left);
    flipHelper(node->right);
    TreeNode<Etype> *temp = NULL;
    temp = node->left;
    node->left = node->right;
    node->right = temp;
    // recurse all the way to the left
    // recurse all the way to the right
    // swap left and right
  }

  //Insert item x into the tree using BST ordering
  virtual bool insert(Etype & x)
  {
    return insert(x, root, NULL);
  }

  TreeNode<Etype> * buildFromPreorderHelper (int &i, Etype* list, TreeNode<Etype> * par)
  {
    TreeNode<Etype> * node = new TreeNode<Etype>(list[i],NULL,NULL,par);
    //std::cout << list[i] << std::endl;
    if (list[i+1] == 2)
    {
      i+=2;
      node->left = buildFromPreorderHelper(i, list, node);
      i+=2;
      node->right = buildFromPreorderHelper(i, list, node);
    }

    else if (list[i+1] == 1)
    {
      i+=2;
      node->left = buildFromPreorderHelper(i, list,node);
    }

      return node;
  }

  void buildFromPreorder(Etype* list) {
    // O(n) = log(n)

    int i = 0;
    root = buildFromPreorderHelper(i,list,NULL);
  };

//Print the tree preceeded by the "msg".
// Print the tree to depth "depth"
// Print the whole tree if no depth is specified
};

/*Return the value of the currentNode */
  template <class Etype>
  Etype BinaryTree<Etype>::getCurrNodeValue( )
  {
    if (currentNode == NULL) return LOW;
    return currentNode->element;
  }


// return the number of nodes in the tree rooted at t
template <class Etype>
int BinaryTree<Etype>::size(TreeNode<Etype> *t) const
{
  if (t == NULL) return 0;
  return (1 + size(t->left) + size(t->right));
}


//Print the contents of tree t
// Indent the tree bby the string "indent"
// Print the tree to a depth of "depth" given "currdepth" is the depth of t
template <class Etype>
void BinaryTree<Etype>::printTree(TreeNode<Etype> *t, std::string indent, int currdepth, int depth)
{
  if (t == NULL || currdepth>depth) return;
  printTree(t->right, indent + "  ", currdepth + 1, depth);
  if (t->parent != NULL)
    std::cout << indent << t->element << "(" << t->parent->element << ")" << std::endl;
  else
    std::cout << indent << t->element << "( no parent)" << std::endl;
  printTree(t->left, indent + "  ", currdepth + 1, depth);
}

// insert inserts data into the tree rooted at t
// parent is the parent of t
// Returns true if insertion is successful
// Code is O(log n) for a balanced tree as each level is accessed once
// This inserts as if we wanted to created a binary search tree.
template <class Etype>
bool BinaryTree<Etype>::insert(Etype & data, TreeNode<Etype> * & t, TreeNode<Etype> *  parent)
{
  if (t == NULL)
  {  // cout << "inserting " << data << endl;
    t = new TreeNode<Etype>(data, NULL, NULL, parent);
    if (t == NULL) return false;
    return true;
  }
  if (data < t->element) return insert(data, t->left, t);
  return insert(data, t->right, t);
}
// find seaches for data in the tree rooted at curr
// Returns the node if the find is successful, NULL otherwise
// Code is O(log n) for a balanced tree as each level is accessed once








